%global srcname justbytes

Summary:        Library for handling computation with address ranges in bytes
Name:           python-%{srcname}
Version:        0.15.2
Release:        5%{?dist}
License:        LGPLv2+
URL:            http://pypi.python.org/pypi/justbytes
Source0:        https://pypi.io/packages/source/j/%{srcname}/%{srcname}-%{version}.tar.gz

BuildArch:      noarch

%description
A library for handling computations with address ranges. The library also offers a configurable way
to extract the representation of a value.

%package -n python3-%{srcname}
Summary:        Library for handling computation with address ranges in bytes
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools

%description -n python3-%{srcname}
A library for handling computations with address ranges. The library also offers a configurable way
to extract the representation of a value.

Python 3 version.

%prep
%autosetup -n %{srcname}-%{version}
rm -rf justbytes.egg-info

%build
%py3_build

%install
%py3_install

%files -n python3-%{srcname}
%license LICENSE
%doc README.rst
%{python3_sitelib}/justbytes/
%{python3_sitelib}/justbytes-%{version}-*.egg-info/

%changelog
* Thu Sep 26 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.15.2-5
- Rebuilt for clarifying the packages requirement in BaseOS and AppStream

* Fri Aug 16 2024 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.15.2-4
- Rebuilt for loongarch release

* Tue Sep 19 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.15.2-3
- Rebuilt for python 3.11

* Fri Sep 08 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.15.2-2
- Rebuilt for OpenCloudOS Stream 23.09

* Mon Jul 10 2023 Shuo Wang <abushwang@tencent.com> - 0.15.2-1
- update to 0.15.2

* Fri Apr 28 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.15-3
- Rebuilt for OpenCloudOS Stream 23.05

* Fri Mar 31 2023 OpenCloudOS Release Engineering <releng@opencloudos.tech> - 0.15-2
- Rebuilt for OpenCloudOS Stream 23

* Thu Mar 16 2023 Shuo Wang <abushwang@tencent.com> - 0.15-1
- initial build
